provider "local" {}

resource "null_resource" "move_files" {
  provisioner "local-exec" {
    command = <<EOT
      cp -r ./../../common ./../../lambda_functions/lambda_layer/python/common
    EOT
  }
}
