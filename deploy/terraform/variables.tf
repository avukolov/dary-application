variable "functions" {
  type = map(object({
    name = string
    resource_name = string
    http_method = string
    timeout = number
    auth = bool
  }))

  default = {
    "get_all_posts_public" = {
      name = "get_all_posts_public"
      resource_name = "public_posts"
      http_method = "GET"
      timeout = 3
      auth = false
    }
    "get_post_public" = {
      name = "get_post_public"
      resource_name = "public_post_id"
      http_method = "GET"
      timeout = 3
      auth = false
    }
    "get_all_posts" = {
      name = "get_all_posts"
      resource_name = "posts"
      http_method = "GET"
      timeout = 3
      auth = true
    }
    "get_post" = {
      name = "get_post"
      resource_name = "post_id"
      http_method = "GET"
      timeout = 3
      auth = true
    }
    "create_post" = {
      name = "create_post"
      resource_name = "posts"
      http_method = "POST"
      timeout = 3
      auth = true
    }
    "update_post" = {
      name = "update_post"
      resource_name = "post_id"
      http_method = "PATCH"
      timeout = 3
      auth = true
    }
    "delete_post" = {
      name = "delete_post"
      resource_name = "post_id"
      http_method = "DELETE"
      timeout = 3
      auth = true
    }
    "log_in_user" = {
      name = "log_in_user"
      resource_name = "auth"
      http_method = "POST"
      timeout = 3
      auth = false
    }
    "sign_up_user" = {
      name = "sign_up_user"
      resource_name = "users"
      http_method = "POST"
      timeout = 3
      auth = false
    }
    "delete_user" = {
      name = "delete_user"
      resource_name = "users"
      http_method = "DELETE"
      timeout = 30
      auth = true
    }
  }
}

variable "dynamo_db_table_name" {
  type    = string
  default = "PostsTable"
}

variable "stage_name" {
  type    = string
  default = "dev"
}

variable "aws_region" {
  type = string
  default = "eu-west-1"
}
