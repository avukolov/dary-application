resource "aws_api_gateway_rest_api" "this" {
  name = "dary_application_api_gateway"
}

resource "aws_cognito_user_pool" "pool" {
  name                      = "dary_application_user_pool"
  auto_verified_attributes  = []

  password_policy {
    minimum_length    = 6
    require_lowercase = true
    require_numbers   = true
    temporary_password_validity_days = 7
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name                = "dary_application_user_pool_client"

  user_pool_id        = aws_cognito_user_pool.pool.id
  generate_secret     = false
  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_PASSWORD_AUTH"
  ]
}

resource "aws_api_gateway_authorizer" "this" {
  name                   = "user_authorizer"
  rest_api_id            = aws_api_gateway_rest_api.this.id
  type                   = "COGNITO_USER_POOLS"
  identity_source        = "method.request.header.Authorization"
  provider_arns = [ aws_cognito_user_pool.pool.arn ]
  authorizer_result_ttl_in_seconds = 300
}

resource "aws_api_gateway_resource" "posts" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "posts"
}

resource "aws_api_gateway_resource" "post_id" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_resource.posts.id
  path_part   = "{id}"
}

resource "aws_api_gateway_resource" "public_posts" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_resource.posts.id
  path_part   = "public"
}

resource "aws_api_gateway_resource" "public_post_id" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_resource.public_posts.id
  path_part   = "{id}"
}

resource "aws_api_gateway_resource" "auth" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "auth"
}

resource "aws_api_gateway_resource" "users" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "users"
}

locals {
  api_gateway_resources = {
    posts   = aws_api_gateway_resource.posts
    post_id   = aws_api_gateway_resource.post_id
    public_posts   = aws_api_gateway_resource.public_posts
    public_post_id = aws_api_gateway_resource.public_post_id
    auth = aws_api_gateway_resource.auth
    users = aws_api_gateway_resource.users
  }
}

resource "aws_api_gateway_method" "this" {
  for_each = var.functions

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = local.api_gateway_resources[each.value.resource_name].id
  http_method   = each.value.http_method

  authorization = each.value.auth == true ? "COGNITO_USER_POOLS" : "NONE"
  authorizer_id = each.value.auth == true ? aws_api_gateway_authorizer.this.id : null

  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_integration" "integration" {
  for_each = var.functions

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = local.api_gateway_resources[each.value.resource_name].id
  http_method             = aws_api_gateway_method.this[each.value.name].http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this[each.key].invoke_arn
}

resource "aws_lambda_permission" "lambda_permission" {
  for_each = var.functions

  action        = "lambda:InvokeFunction"
  function_name = each.key
  principal     = "apigateway.amazonaws.com"

  # The /* part allows invocation from any stage, method and resource path
  # within API Gateway.
  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/${each.value.http_method}${local.api_gateway_resources[each.value.resource_name].path}"

  depends_on = [ aws_lambda_function.this ]
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id

  triggers = {
    # NOTE: The configuration below will satisfy ordering considerations,
    #       but not pick up all future REST API changes. More advanced patterns
    #       are possible, such as using the filesha1() function against the
    #       Terraform configuration file(s) or removing the .id references to
    #       calculate a hash against whole resources. Be aware that using whole
    #       resources will show a difference after the initial implementation.
    #       It will stabilize to only change when resources change afterwards.
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.posts.id,
      aws_api_gateway_method.this["create_post"].id,
      aws_api_gateway_integration.integration["create_post"].id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "this" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.this.id
  stage_name    = var.stage_name
}

output "base_url" {
  value = aws_api_gateway_stage.this.invoke_url
}
