resource "aws_iam_role" "lambda_execution_role" {
  name               = "lambda_execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "lambda_policy" {
  name        = "lambda_policy"
  description = "Policy for Lambda execution role to access Cognito and DynamoDB"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "cognito-idp:AdminConfirmSignUp",
          "cognito-idp:AdminDeleteUser",
          "cognito-idp:InitiateAuth",
          "cognito-idp:SignUp",
          "dynamodb:Scan",
          "dynamodb:Query",
          "dynamodb:PutItem",
          "dynamodb:GetItem",
          "dynamodb:UpdateItem",
          "dynamodb:DeleteItem"
        ]
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "cognito_execution_role_attachment" {
  role       = aws_iam_role.lambda_execution_role.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}

data "archive_file" "lambda_layer" {
  type        = "zip"
  source_dir  = "${path.module}/../../lambda_functions/lambda_layer"
  output_path = "lambda_layer_payload.zip"
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "lambda_layer_payload.zip"
  layer_name = "dary-application-lambda-layer"

  source_code_hash = data.archive_file.lambda_layer.output_base64sha256
  compatible_runtimes = ["python3.10"]
}

data "archive_file" "this" {
  for_each = var.functions

  type        = "zip"
  source_file = "${path.module}/../../lambda_functions/${each.value.name}.py"
  output_path = "${each.value.name}.zip"
}

resource "aws_lambda_function" "this" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  for_each = var.functions


  filename      = "${each.value.name}.zip"
  function_name = each.value.name
  role          = aws_iam_role.lambda_execution_role.arn
  handler       = "${each.value.name}.handler"

  layers = [ aws_lambda_layer_version.lambda_layer.arn ]

  source_code_hash = data.archive_file.this[each.key].output_base64sha256

  runtime = "python3.10"


  timeout = each.value.timeout
  environment {
    variables = {
      COGNITO_USER_POOL_CLIENT_ID: aws_cognito_user_pool_client.client.id
      COGNITO_USER_POOL_ID: aws_cognito_user_pool.pool.id
      DB_TABLE_NAME = var.dynamo_db_table_name
    }
  }
}
