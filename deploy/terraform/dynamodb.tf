resource "aws_dynamodb_table" "posts-table" {
  name           = var.dynamo_db_table_name
  hash_key       = "id"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "user_id"
    type = "S"
  }

  attribute {
    name = "lookup"
    type = "S"
  }

  attribute {
    name = "createdDate"
    type = "S"
  }

  global_secondary_index {
    name               = "UserIdCreatedDateIndex"
    hash_key           = "user_id"
    range_key          = "createdDate"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "ALL"
  }

  global_secondary_index {
    name               = "LookupCreatedDateIndex"
    hash_key           = "lookup"
    range_key          = "createdDate"
    write_capacity     = 1
    read_capacity      = 1
    projection_type    = "ALL"
  }
}
