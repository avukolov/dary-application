import os

import boto3
import botocore
from dotenv import load_dotenv
from shared import stack_parameters


def update_cloudfromation_stack():
    load_dotenv()

    S3_BUCKET_NAME = os.getenv("S3_BUCKET_NAME")

    # Check AWS Credentials
    try:
        boto3.Session(
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_KEY"),
        )
        s3 = boto3.client("s3")
        s3.head_bucket(Bucket=S3_BUCKET_NAME)
    except (
        botocore.exceptions.UnauthorizedSSOTokenError,
        botocore.exceptions.ClientError,
    ) as e:
        print(f"Error: {e}")
        return

    cloudformation_client = boto3.client("cloudformation")
    cf_template = open("deploy/cloudformation/template.yaml").read()

    try:
        response = cloudformation_client.update_stack(
            StackName=os.getenv("CLOUDFORMATION_STACK_NAME"),
            TemplateBody=cf_template,
            Parameters=stack_parameters(),
            Capabilities=["CAPABILITY_IAM"],
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            print("Error updating stack")
            return

        # deploy new changes
        api_getaway_client = boto3.client("apigateway")
        all_apis_response = api_getaway_client.get_rest_apis()

        if response["ResponseMetadata"]["HTTPStatusCode"] != 200:
            print("Error getting rest apis")
            return

        rest_api_id = list(
            filter(lambda el: el["name"] == "PostsApi", all_apis_response["items"])
        )[0]["id"]

        api_getaway_client.create_deployment(
            restApiId=rest_api_id,
            stageName=os.getenv("API_GATEWAY_STAGE_NAME"),
        )

        print(response["StackId"])
    except botocore.exceptions.ClientError as e:
        print(f"Error: {e}")
        return


if __name__ == "__main__":
    update_cloudfromation_stack()
