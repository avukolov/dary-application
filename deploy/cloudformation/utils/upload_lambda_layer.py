import os
import shutil

import boto3
import botocore
from alive_progress import alive_it
from dotenv import load_dotenv
from shared import lambda_functions


def upload_lambda_layer():
    load_dotenv()

    S3_BUCKET_NAME = os.getenv("S3_BUCKET_NAME")

    # Check AWS Credentials
    try:
        boto3.Session(
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_KEY"),
        )
        s3 = boto3.client("s3")
        s3.head_bucket(Bucket=S3_BUCKET_NAME)

        # Copy common folder to lambda layer
        shutil.copytree(
            "common", "lambda_functions/lambda_layer/python/common", dirs_exist_ok=True
        )

        # Archive lambda layer
        archive_name = "lambda_layer.zip"

        print("Start archiving lambda layer!")
        os.chdir("lambda_functions/lambda_layer")
        os.system(f"zip -r -q {archive_name} python")

        # Upload archive to S3
        s3.upload_file(archive_name, S3_BUCKET_NAME, archive_name)

        # Update lambda layer
        lambda_client = boto3.client("lambda")
        response = lambda_client.publish_layer_version(
            LayerName=os.getenv("LAMBDA_LAYER_NAME"),
            Content={"S3Bucket": S3_BUCKET_NAME, "S3Key": archive_name},
        )
        print("Lambda layer published!")

        if response["ResponseMetadata"]["HTTPStatusCode"] != 201:
            raise Exception("Failed to publish lambda layer")

        # Update version of lambda layer in lambda functions
        lambda_function_names = list(lambda_functions().values())

        for function_name in alive_it(lambda_function_names):
            try:
                lambda_client.update_function_configuration(
                    FunctionName=function_name, Layers=[response["LayerVersionArn"]]
                )
                print(f"Updated {function_name} with new layer version")
            except lambda_client.exceptions.ResourceNotFoundException as e:
                print(
                    f"Looks like functions were not created. It's ok when the stack has not created yet: {e}"
                )
    except botocore.exceptions.UnauthorizedSSOTokenError as e:
        print(f"Error: {e}")
    finally:
        # Clean up
        if os.path.exists(archive_name):
            os.remove(archive_name)
        else:
            print(f"{archive_name} does not exist")


if __name__ == "__main__":
    upload_lambda_layer()
