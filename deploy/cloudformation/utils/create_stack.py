import os

import boto3
import botocore
from dotenv import load_dotenv
from shared import stack_parameters


def create_cloudfromation_stack():
    load_dotenv()

    S3_BUCKET_NAME = os.getenv("S3_BUCKET_NAME")

    # Check AWS Credentials
    try:
        boto3.Session(
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_KEY"),
        )
        s3 = boto3.client("s3")
        s3.head_bucket(Bucket=S3_BUCKET_NAME)
    except (
        botocore.exceptions.UnauthorizedSSOTokenError,
        botocore.exceptions.ClientError,
    ) as e:
        print(f"Error: {e}")
        return

    cloudformation_client = boto3.client("cloudformation")
    cf_template = open("deploy/cloudformation/template.yaml").read()

    response = cloudformation_client.create_stack(
        StackName=os.getenv("CLOUDFORMATION_STACK_NAME"),
        TemplateBody=cf_template,
        Parameters=stack_parameters(),
        Capabilities=["CAPABILITY_IAM"],
    )

    print(response["StackId"])


if __name__ == "__main__":
    create_cloudfromation_stack()
