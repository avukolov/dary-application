import os

from dotenv import load_dotenv


def stack_parameters():
    load_dotenv()

    return [
        {
            "ParameterKey": "ApiGatewayStageName",
            "ParameterValue": os.getenv("API_GATEWAY_STAGE_NAME"),
        },
        {
            "ParameterKey": "S3BucketName",
            "ParameterValue": os.getenv("S3_BUCKET_NAME"),
        },
        {
            "ParameterKey": "DynamoDBTableName",
            "ParameterValue": os.getenv("DB_TABLE_NAME"),
        },
        {
            "ParameterKey": "LambdaLayerName",
            "ParameterValue": os.getenv("LAMBDA_LAYER_NAME"),
        },
        {
            "ParameterKey": "PostCreateLambdaFunctionName",
            "ParameterValue": os.getenv("POST_CREATE_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostDeleteLambdaFunctionName",
            "ParameterValue": os.getenv("POST_DELETE_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostGetAllPublicLambdaFunctionName",
            "ParameterValue": os.getenv("POST_INDEX_PUBLIC_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostGetAllLambdaFunctionName",
            "ParameterValue": os.getenv("POST_INDEX_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostGetLambdaFunctionName",
            "ParameterValue": os.getenv("POST_SHOW_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostGetPublicLambdaFunctionName",
            "ParameterValue": os.getenv("POST_SHOW_PUBLIC_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "PostUpdateLambdaFunctionName",
            "ParameterValue": os.getenv("POST_UPDATE_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "LogInUserLambdaFunctionName",
            "ParameterValue": os.getenv("LOG_IN_USER_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "SignUpUserLambdaFunctionName",
            "ParameterValue": os.getenv("SIGN_UP_USER_LAMBDA_NAME"),
        },
        {
            "ParameterKey": "DeleteUserLambdaFunctionName",
            "ParameterValue": os.getenv("DELETE_USER_LAMBDA_NAME"),
        },
    ]


def lambda_functions():
    return {
        "get_all_posts_public": os.getenv("POST_INDEX_PUBLIC_LAMBDA_NAME"),
        "get_all_posts": os.getenv("POST_INDEX_LAMBDA_NAME"),
        "get_post_public": os.getenv("POST_SHOW_PUBLIC_LAMBDA_NAME"),
        "get_post": os.getenv("POST_SHOW_LAMBDA_NAME"),
        "create_post": os.getenv("POST_CREATE_LAMBDA_NAME"),
        "update_post": os.getenv("POST_UPDATE_LAMBDA_NAME"),
        "delete_post": os.getenv("POST_DELETE_LAMBDA_NAME"),
        "log_in_user": os.getenv("LOG_IN_USER_LAMBDA_NAME"),
        "sign_up_user": os.getenv("SIGN_UP_USER_LAMBDA_NAME"),
        "delete_user": os.getenv("DELETE_USER_LAMBDA_NAME"),
    }
