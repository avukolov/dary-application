import os

import boto3
import botocore
from alive_progress import alive_it
from dotenv import load_dotenv
from shared import lambda_functions


def upload_function_changes():
    load_dotenv()

    S3_BUCKET_NAME = os.getenv("S3_BUCKET_NAME")

    # List of functions to be uploaded
    functions = lambda_functions()

    # Check AWS Credentials
    try:
        boto3.Session(
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
            aws_secret_access_key=os.getenv("AWS_SECRET_KEY"),
        )
        s3 = boto3.client("s3")
        s3.head_bucket(Bucket=S3_BUCKET_NAME)
    except (
        botocore.exceptions.UnauthorizedSSOTokenError,
        botocore.exceptions.ClientError,
    ) as e:
        print(f"Error: {e}")
        return

    lambda_client = boto3.client("lambda")

    for file in alive_it(list(functions.keys())):
        # Archive the source code of the function
        archive_name = f"{file}.zip"
        os.system(f"zip -j {archive_name} lambda_functions/{file}.py")

        # Upload changes to S3
        s3.upload_file(archive_name, S3_BUCKET_NAME, archive_name)

        # Update lambda function code
        try:
            lambda_client.update_function_code(
                FunctionName=functions[file],
                S3Bucket=S3_BUCKET_NAME,
                S3Key=archive_name,
                Publish=True,
            )
        except lambda_client.exceptions.ResourceNotFoundException as e:
            print(f"Error: {e}")

        # Clean up
        os.remove(archive_name)

    print("Functions uploaded!")


if __name__ == "__main__":
    upload_function_changes()
