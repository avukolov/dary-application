import json
import os

import boto3
from jsonschema import validate

from common.error_handler import error_handler


@error_handler
def handler(event, context):
    client = boto3.client("cognito-idp")

    body = json.loads(event["body"])

    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "password": {"type": "string"},
        },
        "required": ["username", "password"],
        "additionalProperties": False,
    }

    # Validate params for creating user
    validate(instance=body, schema=schema)

    response = client.sign_up(
        ClientId=os.getenv("COGNITO_USER_POOL_CLIENT_ID"),
        Username=body["username"],
        Password=body["password"],
    )

    # Automatically confirm the user
    client.admin_confirm_sign_up(
        UserPoolId=os.getenv("COGNITO_USER_POOL_ID"), Username=body["username"]
    )

    response_body = {"user_id": response.get("UserSub"), "username": body["username"]}

    return {
        "statusCode": 201,
        "body": json.dumps(response_body),
    }
