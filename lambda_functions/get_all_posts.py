import json

from common.current_user import current_user_id
from common.error_handler import error_handler
from common.post import Post
from common.query import posts_fetcher


@error_handler
def handler(event, context):
    user_id = current_user_id(event["headers"])
    result = posts_fetcher(event, user_id)

    posts = list(map(lambda post_data: Post(**post_data).serialized(), result))

    return {"statusCode": 200, "body": json.dumps(posts, default=str)}
