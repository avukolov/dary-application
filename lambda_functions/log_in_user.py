import json
import os

import boto3
from jsonschema import validate

from common.error_handler import error_handler


@error_handler
def handler(event, context):
    client = boto3.client("cognito-idp")

    cognito_user_pool_client_id = os.getenv("COGNITO_USER_POOL_CLIENT_ID")
    body = json.loads(event["body"])

    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "password": {"type": "string"},
        },
        "required": ["username", "password"],
        "additionalProperties": False,
    }

    # Validate params to log in user
    validate(instance=body, schema=schema)

    response = client.initiate_auth(
        AuthFlow="USER_PASSWORD_AUTH",
        AuthParameters={"USERNAME": body["username"], "PASSWORD": body["password"]},
        ClientId=cognito_user_pool_client_id,
    )

    return {
        "statusCode": 201,
        "body": json.dumps(response["AuthenticationResult"]),
    }
