import json

from common.current_user import current_user_id
from common.custom_errors import PostNotFoundError
from common.dynamodb import post_table
from common.error_handler import error_handler
from common.post import Post


@error_handler
def handler(event, context):
    post_id = event["pathParameters"]["id"]
    user_id = current_user_id(event["headers"])

    response = post_table().get_item(Key={"id": post_id})

    if response.get("Item") is None:
        raise PostNotFoundError("Post not found")
    elif response.get("Item")["user_id"] != user_id:
        raise PostNotFoundError("Post not found")
    else:
        post = Post(**response.get("Item"))
        result = {"statusCode": 200, "body": json.dumps(post.serialized(), default=str)}

    return result
