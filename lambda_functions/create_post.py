import json

from jsonschema import validate

from common.current_user import current_user_id
from common.dynamodb import post_table
from common.error_handler import error_handler
from common.post import Post


@error_handler
def handler(event, context):
    user_id = current_user_id(event["headers"])
    post_create = json.loads(event["body"])

    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string", "minLength": 1, "maxLength": 200},
            "body": {"type": "string", "minLength": 1, "maxLength": 2000},
            "tags": {"type": "array", "items": {"type": "string"}},
        },
        "required": ["title", "body"],
        "additionalProperties": False,
    }

    # Validate params for creating post
    validate(instance=post_create, schema=schema)

    # Build Post instance
    post = Post(**post_create, user_id=user_id)

    # Prepare dict for storing in DynamoDB
    post_table().put_item(Item=json.loads(post.model_dump_json()))

    return {
        "statusCode": 201,
        "body": json.dumps(post.serialized(), default=str),
    }
