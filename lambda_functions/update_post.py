import json
from datetime import datetime

from common.current_user import current_user_id
from common.dynamodb import post_table
from common.error_handler import error_handler
from common.update_post_params import UpdatePostParams


@error_handler
def handler(event, context):
    post_id = event["pathParameters"]["id"]
    user_id = current_user_id(event["headers"])

    update_body = json.loads(event["body"])

    # Validate params for updating post
    params = UpdatePostParams(**update_body)

    if "tags" in update_body.keys():
        # Allow to update tags to empty list
        # The default value for tags is an empty list,
        # so we can't use `exclude_defaults=True`
        post_update = params.model_dump(exclude_none=True)
    else:
        post_update = params.model_dump(exclude_defaults=True)

    # Update updateDate
    post_update.update({"updatedDate": str(datetime.now())})

    update_expression = "SET " + ", ".join(f"{k}=:{k}" for k in post_update.keys())
    expression_attribute_values = {f":{k}": v for k, v in post_update.items()}
    expression_attribute_values.update({":id": post_id, ":user_id": user_id})
    post_table().update_item(
        Key={"id": post_id},
        UpdateExpression=update_expression,
        ConditionExpression="id = :id AND user_id = :user_id",
        ExpressionAttributeValues=expression_attribute_values,
    )
    return {"statusCode": 204, "body": ""}
