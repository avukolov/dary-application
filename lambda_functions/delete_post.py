from common.current_user import current_user_id
from common.dynamodb import post_table
from common.error_handler import error_handler


@error_handler
def handler(event, context):
    post_id = event["pathParameters"]["id"]
    user_id = current_user_id(event["headers"])

    post_table().delete_item(
        Key={"id": post_id},
        ConditionExpression="attribute_exists (id) AND user_id = :user_id",
        ExpressionAttributeValues={":user_id": user_id},
    )

    return {"statusCode": 204, "body": ""}
