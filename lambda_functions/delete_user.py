import os

import boto3

from common.current_user import current_user_id, current_user_username
from common.dynamodb import post_table
from common.error_handler import error_handler

LIMIT = 50


def delete_user_posts(user_id):
    last_evaluated_key = None
    remaining_posts = total_user_posts(user_id).get("Count", 0)
    while remaining_posts > 0:
        scan_params = {
            "FilterExpression": boto3.dynamodb.conditions.Attr("user_id").eq(user_id),
            "Limit": LIMIT,
        }
        if last_evaluated_key:
            scan_params.update({"ExclusiveStartKey": last_evaluated_key})
        response = post_table().scan(**scan_params)
        for item in response["Items"]:
            post_id = item.get("id")
            post_table().delete_item(Key={"id": post_id})
        last_evaluated_key = response.get("LastEvaluatedKey")
        remaining_posts = total_user_posts(user_id).get("Count", 0)


def total_user_posts(user_id):
    return post_table().scan(
        FilterExpression=boto3.dynamodb.conditions.Attr("user_id").eq(user_id),
        Select="COUNT",
    )


@error_handler
def handler(event, context):
    headers = event["headers"]
    username = current_user_username(headers)
    user_id = current_user_id(headers)

    delete_user_posts(user_id)

    client = boto3.client("cognito-idp")
    client.admin_delete_user(
        UserPoolId=os.getenv("COGNITO_USER_POOL_ID"),
        Username=username,
    )

    return {"statusCode": 204, "body": ""}
