import json

from common.error_handler import error_handler
from common.post import Post
from common.query import posts_fetcher


@error_handler
def handler(event, context):
    result = posts_fetcher(event)
    posts = list(map(lambda post_data: Post(**post_data).serialized(), result))

    return {"statusCode": 200, "body": json.dumps(posts, default=str)}
