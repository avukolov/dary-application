# Dary Application

## Overview

Dary Application is a study tool designed to enhance serverless knowledge by leveraging AWS services such as CloudFormation, DynamoDB, API Gateway, and Lambda as well as Terraform. It provides a RESTful API for creating, fetching, updating, and deleting posts.

API documentation can be found in `swagger.yaml`.

## Table of Contents
- [Getting Started](#getting-started)
- [Installation](#installation)
  - [Create Virtual Environment](#1-create-virtual-environment)
  - [Activate Virtual Environment](#2-activate-virtual-environment)
  - [Install Packages](#3-install-packages)
  - [Configure Environment Variables](#4-configure-environment-variables)
- [Development](#development)
  - [CloudFormation](#cloudformation)
    - [Update CloudFormation Stack](#update-cloudformation-stack)
    - [Update Lambda Functions](#update-lambda-functions)
    - [Update Lambda Layer](#update-lambda-layer)
  - [Terraform](#terraform)
    - [Create or Update resources](#create-or-update-resources)
    - [Update Lambda functions or Lambda layer code](#update-lambda-functions-or-lambda-layer-code)
- [Authorization](#authorization)
- [Testing](#testing)
  - [Unit Tests](#unit-tests)
  - [API Tests](#api-tests)
- [CloudFormation Deployment Steps](#cloudformation-deployment-steps)
- [Terraform Deployment Steps](#terraform-deployment-steps)

## Getting Started

The Dary Application aims to facilitate the learning of serverless architectures. It utilizes the following AWS services:
- **AWS CloudFormation:** For infrastructure as code.
- **DynamoDB:** For data storage.
- **API Gateway:** For exposing RESTful API endpoints.
- **AWS Cognito:** For authorization.
- **AWS Lambda:** For serverless compute logic.
- **Terraform** As alternative way for mantaining cloud infrastructure

This application allows you to:
- Fetch public posts.
- Create posts.
- Fetch posts.
- Update posts.
- Delete posts.
- Create user.
- Sign In user.

## Installation

### 1. Create Virtual Environment

To isolate the project dependencies, create a virtual environment:

```bash
python3.10 -m venv env
```

### 2. Activate Virtual Environment

Activate the virtual environment:

```bash
source env/bin/activate
```

### 3. Install Packages

#### Development and Testing Packages
Install the packages required for development and testing:

```bash
pip3 install -r dev-requirements.txt
```

#### Production Packages
Install the packages required for production:

```bash
pip3 install -r lambda_functions/lambda_layer/requirements.txt --platform manylinux2014_x86_64 --target lambda_functions/lambda_layer/python --implementation cp --python-version 3.10 --only-binary=:all:
```

### 4. Configure Environment Variables

Copy the example environment variables file to create your local configuration:
```bash
cp .env_example .env
```

## Development

### Lambda Functions

All Lambda functions are located in the top level of the `lambda_functions` folder. Each function corresponds to a specific Lambda function.

### Shared Code

Reusable code is placed in the `common` folder.

### CloudFormation

#### Update CloudFormation Stack

To make some changes to the cloud infrastructure, after updating `deploy/cloudformation/template.yaml` you can implement them with:
```bash
python3.10 deploy/cloudformation/utils/deploy/update_stack.py
```
This script updates the stack and creates a new deployment, so the changes will be available in the API.

#### Update Lambda Functions

To update Lambda functions with new code versions, use the following script. It retrieves the latest version of the Lambda function code, archives it, uploads it to S3, updates the Lambda function code on AWS, and deploys the new version of the Lambda function:

```bash
python3.10 deploy/cloudformation/utils/upload_lambda_functions.py
```

#### Update Lambda Layer

To update shared models or production packages, update the Lambda Layer using the following script. It creates an archive with production packages and models, uploads it to S3, publishes a new version of the Lambda Layer, and updates Lambda functions with the new version of the Lambda Layer:

```bash
python3.10 deploy/cloudformation/utils/upload_lambda_layer.py
```


### Terraform

The application can be deployed to AWS via Terraform too.
The Terraform config placed in `deploy/terraform` folder

#### Create or Update resources

Create resources on AWS, first run `plan` command
```bash
terraform -chdir=deploy/terraform plan
```

and after that use `apply`
```bash
terraform -chdir=deploy/terraform apply
```

#### Update Lambda functions or Lambda layer code

The Terraform config for Lambda functions and Lamda Layer include `source_code_hash`, so all changes in source code of the functions can be applied via:
```bash
terraform -chdir=deploy/terraform apply
```

## Authorization

The API has only several public endpoints. All other endpoints for manipulating with `Posts` are protected by authentication.
The `Post` public endpoints are:
```python
GET /posts/public
GET /posts/public/{id}
```
Protected endpoints are:
```python
GET /posts
GET /posts/{id}
POST /posts
PATCH /posts/{id}
DELETE /posts/{id}
```
You need to create a user and get an auth token to be able to work with protected endpoints.
```python
POST /users # creates a user
POST /auth # provides auth token
```

Using `IdToken` from response of `POST /auth` you need to use it as Bearer token to all protected endpoints.
So, curl representation of request to create `Post` will looks like:
```bash
curl --location 'https://the-awesome-app.execute-api.eu-west-1.amazonaws.com/v1/posts' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer {{IdToken}}' \
--data '{
  "title": "New post",
  "body": "This is an example post",
  "tags": ["test", "demo"]
}'
```

## Testing

The application's functionality is covered by unit tests and API/request tests.

### Unit Tests

Unit tests are located in the `tests` folder.

To run unit tests:

```bash
pytest tests
```

To check test coverage:

```bash
pytest --cov=lambda_functions --cov=common tests
```

### API Tests

The Postman collection `Dary Application.postman_collection.json` contains a list of API requests.
After creating the CloudFormation stack, obtain the output of the created stack (ApiEndpoint). Replace the Postman application variable `baseUrl` with ApiEndpoint.
You can then run the collection (all folders except Debugging) and observe the results.


## Deployment

> ### Preconditions
> The deployment scripts rely on boto3 and require authorization on AWS. `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` using for authorization.
> Here are list of reqired зermissions policies that AWS user needs to run deploy scripts:
> **CloudFormation:**
> - AmazonAPIGatewayAdministrator
> - AmazonCognitoPowerUser
> - AmazonDynamoDBFullAccess
> - AmazonS3FullAccess
> - AWSCloudFormationFullAccess
> - AWSLambda_FullAccess
> - IAMFullAccess
>
> **Terraform:**
> - AmazonAPIGatewayAdministrator
> - AmazonCognitoPowerUser
> - AmazonDynamoDBFullAccess
> - AWSLambda_FullAccess
> - IAMFullAccess

Deployment via CloudFormation uses template `deploy/cloudformation/template.yaml`. The stack name, S3 bucket name, Lambda Layer name, and Lambda functions are configured via parameters defined in `.env`.
Deployment via Terraform uses the following configs:
- `deploy/terraform/api_gateway.tf` - describe API Gateway Resources, Methods, Cognito Pool, connecting API endpoints to Lambda Functions
- `deploy/terraform/dynamodb.tf` - describe DynamoDB
- `deploy/terraform/lambda.tf` - describe Lambda functions and Lambda Layer
- `deploy/terraform/prepare_lambda_layer.tf` - helper to prepare lambda layer folder
- `deploy/terraform/main.tf` - general config (provider, AWS region)
- `deploy/terraform/variables.tf` - variables to define AWS Lambda Functions, DynamoDB table name, API Gateway Stage name

### CloudFormation Deployment Steps

1. Check Environment Variables

Ensure that `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `S3_BUCKET_NAME` are set to valid values in `.env`.

2. Upload Lambda Functions to S3

```bash
python3.10 deploy/cloudformation/utils/upload_lambda_functions.py
```

3. Upload Lambda Layer to S3

```bash
python3.10 deploy/cloudformation/utils/upload_lambda_layer.py
```

4. Create a Stack

``` bash
python3.10 deploy/cloudformation/utils/create_stack.py
```

5. Verify Stack Creation

Check via the AWS console that the stack has been created successfully. If everything is correct, obtain the stack's output (ApiEndpoint) and use the application.

### Terraform Deployment Steps

1. Plan changes
```bash
terraform -chdir=deploy/terraform plan
```

2. Apply changes
```bash
terraform -chdir=deploy/terraform apply
```

3. Get API Gateway envoke url In `Outputs`
When `terraform -chdir=deploy/terraform apply` finished the end of output will look like:
```bash
Outputs:

base_url = "https://eg9ttugx2e.execute-api.eu-west-1.amazonaws.com/dev"
```
