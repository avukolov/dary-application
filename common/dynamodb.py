import os

import boto3
from dotenv import load_dotenv


def post_table():
    load_dotenv()

    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(os.getenv("DB_TABLE_NAME"))
    return table
