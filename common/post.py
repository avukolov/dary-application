import json
from datetime import datetime
from uuid import UUID, uuid4

from pydantic import BaseModel, Field


class Post(BaseModel):
    id: UUID = Field(default_factory=lambda: uuid4())
    user_id: str
    lookup: str = "public"
    title: str
    body: str
    tags: list[str] = []
    createdDate: datetime = Field(default_factory=datetime.now)
    updatedDate: datetime = Field(default_factory=datetime.now)

    def serialized(self):
        return self.model_dump(exclude={"user_id", "lookup"})
