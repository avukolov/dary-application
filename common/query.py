from common.dynamodb import post_table


def get_query_params(expression_attribute_value):
    index_name = (
        "LookupCreatedDateIndex"
        if expression_attribute_value == "public"
        else "UserIdCreatedDateIndex"
    )
    key_condition_expression = (
        "lookup = :lookup"
        if expression_attribute_value == "public"
        else "user_id = :user_id"
    )
    expression_attribute_key = (
        ":lookup" if expression_attribute_value == "public" else ":user_id"
    )

    return {
        "IndexName": index_name,
        "KeyConditionExpression": key_condition_expression,
        "ExpressionAttributeValues": {
            expression_attribute_key: expression_attribute_value
        },
        "ScanIndexForward": True,
    }


def parse_query_parameters(request_query_parameters):
    tags = (
        request_query_parameters.get("tags").split(",")
        if request_query_parameters and request_query_parameters.get("tags")
        else None
    )
    limit = (
        int(request_query_parameters.get("limit"))
        if request_query_parameters and request_query_parameters.get("limit")
        else None
    )
    return tags, limit


def build_filter_expression(tags):
    filter_expression = " OR ".join(
        [f"contains(#tags, :tag{i})" for i in range(1, len(tags) + 1)]
    )
    tags_expression_attribute_values = {
        f":tag{i}": tag for i, tag in enumerate(tags, start=1)
    }
    return filter_expression, tags_expression_attribute_values


def posts_fetcher(event, user_id=None):
    request_query_parameters = event.get("queryStringParameters")
    tags, limit = parse_query_parameters(request_query_parameters)

    expression_attribute_value = user_id if user_id is not None else "public"
    query_params = get_query_params(expression_attribute_value)

    if tags:
        filter_expression, tags_expression_attribute_values = build_filter_expression(
            tags
        )
        expression_attribute_values = query_params["ExpressionAttributeValues"].copy()
        expression_attribute_values.update(tags_expression_attribute_values)

        query_params.update(
            {
                "FilterExpression": filter_expression,
                "ExpressionAttributeNames": {"#tags": "tags"},
                "ExpressionAttributeValues": expression_attribute_values,
            }
        )

    if limit is not None:
        if not tags:
            query_params.update({"Limit": limit})
            result = post_table().query(**query_params).get("Items")
        else:
            response = post_table().query(**query_params)
            result = response.get("Items")[:limit]
    else:
        result = post_table().query(**query_params).get("Items")

    return result
