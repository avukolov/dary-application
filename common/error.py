import json

from botocore.exceptions import ClientError
from jsonschema import ValidationError as JsonSchemaValidationError

from common.custom_errors import PostNotFoundError


class Error:
    def __init__(self, code, error_object):
        self.code = code
        self.error_object = error_object

    def format_pydantic_error(self):
        return [
            self._format_pydantic_error_message(err)
            for err in json.loads(self.error_object.json())
        ]

    def _format_pydantic_error_message(self, err):
        loc = None
        if len(err.get("loc", [])) >= 1:
            loc = err.get("loc", [])[0]

        msg = {
            "string_too_long": f"{loc}: is too long",
            "string_too_short": f"{loc}: is too short",
            "list_type": f"{loc}: is not valid type",
        }.get(err.get("type"), err.get("ctx", {}).get("error", ""))
        return msg

    def format_jsonschema_error(self):
        loc = self._get_jsonschema_error_location(self.error_object)
        msg = {
            "required": lambda e: e.message.split("'")[-1].strip(),
            "minLength": lambda e: "should be non-empty",
            "maxLength": lambda e: "is too long",
            "type": lambda e: "is not valid type",
        }.get(self.error_object.validator, lambda e: "is invalid")(self.error_object)
        return [f"{loc}: {msg}"]

    def _get_jsonschema_error_location(self, jsonschema_error):
        if jsonschema_error.path:
            return jsonschema_error.path[0]
        return jsonschema_error.message.split("'")[1]

    def _prepare_error_messages(self):
        if isinstance(self.error_object, ValueError):
            return self.format_pydantic_error()
        elif isinstance(self.error_object, JsonSchemaValidationError):
            return self.format_jsonschema_error()
        elif isinstance(self.error_object, PostNotFoundError):
            return [self.error_object.args[0]]
        elif isinstance(self.error_object, ClientError):
            error_code = self.error_object.response["Error"]["Code"]
            error_message = self.error_object.response["Error"]["Message"]
            return [f"{error_code}: {error_message}"]
        return [f"Unknown error, ctx: {self.error_object}"]

    def response(self):
        errors = [{"message": msg} for msg in self._prepare_error_messages()]
        return {
            "statusCode": self.code,
            "body": json.dumps({"errors": errors}),
        }
