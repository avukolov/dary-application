from pydantic import BaseModel, Field, model_validator
from typing import Optional, List


class UpdatePostParams(BaseModel):
    title: Optional[str] = Field(None, min_length=1, max_length=200)
    body: Optional[str] = Field(None, min_length=1, max_length=2000)
    tags: List[str] = Field(default_factory=list)

    @model_validator(mode="before")
    def at_least_one_key_present(cls, data):
        if not any(key in ["title", "body", "tags"] for key in list(data.keys())):
            raise ValueError(
                "At least one of 'title', 'body', or 'tags' must be present."
            )
        return data
