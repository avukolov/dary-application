import jsonschema
from botocore.exceptions import ClientError
from pydantic import ValidationError

from common.custom_errors import PostNotFoundError
from common.error import Error


def handle_value_error(e):
    return Error(400, e).response()


def handle_client_error(e):
    error_code = e.response["Error"]["Code"]
    if error_code == "InvalidParameterException":
        return Error(400, e).response()
    elif error_code == "UserNotFoundException":
        return Error(400, e).response()
    elif error_code == "UsernameExistsException":
        return Error(400, e).response()
    elif error_code == "NotAuthorizedException":
        return Error(400, e).response()
    elif error_code == "ConditionalCheckFailedException":
        return Error(404, PostNotFoundError("Post not found")).response()
    else:
        return Error(500, e).response()


def handle_jsonschema_error(e):
    return Error(400, e).response()


def handle_post_not_found_error(e):
    return Error(404, e).response()


def handle_generic_error(e):
    return Error(500, e).response()


def error_handler(func):
    def wrapper(event, context):
        try:
            return func(event, context)
        except (ValueError, ValidationError) as e:
            return handle_value_error(e)
        except ClientError as e:
            return handle_client_error(e)
        except jsonschema.exceptions.ValidationError as e:
            return handle_jsonschema_error(e)
        except PostNotFoundError as e:
            return handle_post_not_found_error(e)
        except Exception as e:
            return handle_generic_error(e)

    return wrapper
