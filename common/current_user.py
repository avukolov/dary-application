import jwt


def decode_token(headers):
    # Extract JWT token from Authorization header
    token = headers["Authorization"].split()[1]
    # Decode the token (no verification needed as it's already verified by API Gateway)
    claims = jwt.decode(token, options={"verify_signature": False})
    return claims


def current_user_id(headers):
    return decode_token(headers).get("sub")


def current_user_username(headers):
    return decode_token(headers).get("cognito:username")
