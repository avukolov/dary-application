import pytest
from pydantic import ValidationError

from common.update_post_params import UpdatePostParams


def test_valid_update_post_params():
    title = "Post title"
    body = "Some body"
    tags = ["tag1", "tag2"]

    params_with_title = UpdatePostParams(title=title)
    assert params_with_title.title == title
    assert params_with_title.body is None
    assert params_with_title.tags == []

    params_with_body = UpdatePostParams(body=body)
    assert params_with_body.title is None
    assert params_with_body.body == body
    assert params_with_body.tags == []

    params_with_title_and_body = UpdatePostParams(title=title, body=body)
    assert params_with_title_and_body.title == title
    assert params_with_title_and_body.body == body
    assert params_with_title_and_body.tags == []

    params_with_tags = UpdatePostParams(tags=tags)
    assert params_with_tags.title is None
    assert params_with_tags.body is None
    assert params_with_tags.tags == tags

    params = {"id": "new_id", "title": title, "body": body, "tags": tags}
    params_with_unsopported_keys = UpdatePostParams(**params)
    expected_attributes = {"title": title, "body": body, "tags": tags}
    assert params_with_unsopported_keys.model_dump() == expected_attributes


def test_invalid_update_post_params():
    with pytest.raises(ValidationError) as exc_info:
        UpdatePostParams()
    validation_errors = exc_info.value.errors()
    assert len(validation_errors) == 1
    assert validation_errors[0].get("type") == "value_error"
    assert (
        validation_errors[0].get("msg")
        == "Value error, At least one of 'title', 'body', or 'tags' must be present."
    )
    assert validation_errors[0].get("loc") == ()


@pytest.mark.parametrize(
    "title,body,loc,error_type,error_msg",
    [
        (
            "",
            "Some body",
            "title",
            "string_too_short",
            "String should have at least 1 character",
        ),
        (
            "Title post",
            "",
            "body",
            "string_too_short",
            "String should have at least 1 character",
        ),
        (
            "a" * 201,
            "Some body",
            "title",
            "string_too_long",
            "String should have at most 200 characters",
        ),
        (
            "Title post",
            "a" * 2001,
            "body",
            "string_too_long",
            "String should have at most 2000 characters",
        ),
    ],
)
def test_title_body_length_validation(title, body, loc, error_type, error_msg):
    with pytest.raises(ValidationError) as exc_info:
        UpdatePostParams(title=title, body=body)
    validation_errors = exc_info.value.errors()
    assert len(validation_errors) == 1
    assert validation_errors[0].get("type") == error_type
    assert validation_errors[0].get("msg") == error_msg
    assert validation_errors[0].get("loc") == (loc,)
