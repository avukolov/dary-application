import json
import uuid
from datetime import datetime
from uuid import UUID

from common.post import Post


def test_post_model():
    user_id = str(uuid.uuid4())
    title = "Post title"
    body = "Post body"
    tag_1 = "tag1"
    tag_2 = "tag2"

    post_model = Post(user_id=user_id, title=title, body=body, tags=[tag_1, tag_2])

    assert type(post_model.id) is UUID

    assert post_model.user_id == user_id
    assert post_model.lookup == "public"
    assert post_model.title == title
    assert post_model.body == body
    assert post_model.tags == [tag_1, tag_2]

    assert type(post_model.createdDate) is datetime
    assert type(post_model.updatedDate) is datetime


def test_serialized():
    user_id = str(uuid.uuid4())
    title = "Post title"
    body = "Post body"
    tag_1 = "tag1"
    tag_2 = "tag2"

    post_model = Post(user_id=user_id, title=title, body=body, tags=[tag_1, tag_2])

    expected_serialized_object = {
        "id": post_model.id,
        "title": title,
        "body": body,
        "tags": [tag_1, tag_2],
        "createdDate": post_model.createdDate,
        "updatedDate": post_model.updatedDate,
    }

    assert type(post_model.serialized()) is dict
    assert post_model.serialized() == expected_serialized_object
