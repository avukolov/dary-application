import uuid

import pytest
from faker import Faker

from common.current_user import current_user_id, current_user_username
from tests.utils.helpers import prepare_token

fake = Faker()

user_id = str(uuid.uuid4())
username = fake.user_name()


@pytest.fixture()
def headers():
    token = prepare_token(user_id=user_id, username=username)
    return {"Authorization": f"Bearer {token}"}


def test_current_user_id(headers):
    assert current_user_id(headers) == user_id


def test_current_user_username(headers):
    assert current_user_username(headers) == username
