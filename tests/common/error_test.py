import json

import pytest
from botocore.exceptions import ClientError
from jsonschema import ValidationError as JsonSchemaValidationError
from pydantic import BaseModel, ValidationError, constr
from pydantic import ValidationError as PydanticValidationError

from common.custom_errors import PostNotFoundError
from common.error import Error
from common.update_post_params import UpdatePostParams


@pytest.mark.parametrize(
    "params, expected_messages",
    [
        ({"title": "", "body": ""}, ["title: is too short", "body: is too short"]),
        ({"title": "", "body": "some value"}, ["title: is too short"]),
        ({"title": "some value", "body": ""}, ["body: is too short"]),
        ({"title": "a" * 201, "body": "some value"}, ["title: is too long"]),
        ({"title": "some value", "body": "a" * 2001}, ["body: is too long"]),
    ],
)
def test_format_pydantic_error(params, expected_messages):
    try:
        UpdatePostParams(**params)
    except ValidationError as e:
        error = Error(400, e)
        formatted_errors = error.format_pydantic_error()
        assert formatted_errors == expected_messages


def test_format_jsonschema_error():
    schema = {
        "type": "object",
        "properties": {
            "name": {"type": "string", "maxLength": 5},
        },
        "required": ["name"],
    }

    instance = {"name": "too_long_name"}

    try:
        import jsonschema

        jsonschema.validate(instance, schema)
    except JsonSchemaValidationError as e:
        error = Error(400, e)
        formatted_errors = error.format_jsonschema_error()
        assert formatted_errors == ["name: is too long"]


def test_post_not_found_error():
    error = Error(404, PostNotFoundError("Post not found"))
    response = error.response()
    expected_response = {
        "statusCode": 404,
        "body": json.dumps({"errors": [{"message": "Post not found"}]}),
    }
    assert response == expected_response


def test_client_error():
    boto_error = ClientError(
        {"Error": {"Code": "Some error code", "Message": "Detailed error message"}},
        "OperationName",
    )
    error = Error(500, boto_error)
    response = error.response()
    expected_response = {
        "statusCode": 500,
        "body": json.dumps(
            {"errors": [{"message": "Some error code: Detailed error message"}]}
        ),
    }
    assert response == expected_response


def test_unknown_error():
    error = Error(500, Exception("Unknown error"))
    response = error.response()
    expected_response = {
        "statusCode": 500,
        "body": json.dumps(
            {"errors": [{"message": "Unknown error, ctx: Unknown error"}]}
        ),
    }
    assert response == expected_response
