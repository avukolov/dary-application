import json

import pytest
from botocore.exceptions import ClientError
from jsonschema import validate

from common.custom_errors import PostNotFoundError
from common.error_handler import error_handler
from common.update_post_params import UpdatePostParams


def test_value_error_handling():
    @error_handler
    def func(event, context):
        UpdatePostParams()

    response = func({}, {})
    assert response == {
        "statusCode": 400,
        "body": "{\"errors\": [{\"message\": \"At least one of 'title', 'body', or 'tags' must be present.\"}]}",
    }


def test_validation_error_handling():
    @error_handler
    def func(event, context):
        UpdatePostParams(title="a" * 201)

    response = func({}, {})
    assert response == {
        "statusCode": 400,
        "body": '{"errors": [{"message": "title: is too long"}]}',
    }


@pytest.mark.parametrize(
    "expected_status_code, error_code, expected_message",
    [
        (400, "InvalidParameterException", "InvalidParameterException: Some message"),
        (400, "UserNotFoundException", "UserNotFoundException: Some message"),
        (400, "UsernameExistsException", "UsernameExistsException: Some message"),
        (400, "NotAuthorizedException", "NotAuthorizedException: Some message"),
        (404, "ConditionalCheckFailedException", "Post not found"),
        (500, "SomeOtherError", "SomeOtherError: Some message"),
    ],
)
def test_client_error_handling(expected_status_code, error_code, expected_message):
    @error_handler
    def func(event, context):
        error_response = {"Error": {"Code": error_code, "Message": "Some message"}}
        raise ClientError(error_response, "OperationName")

    expected_body = json.dumps({"errors": [{"message": expected_message}]})

    response = func({}, {})
    assert response == {
        "statusCode": expected_status_code,
        "body": expected_body,
    }


def test_jsonschema_validation_error_handling():
    @error_handler
    def func(event, context):
        schema = {
            "type": "object",
            "properties": {
                "title": {"type": "string", "minLength": 1, "maxLength": 200},
                "body": {"type": "string", "minLength": 1, "maxLength": 2000},
                "tags": {"type": "array", "items": {"type": "string"}},
            },
            "required": ["title", "body"],
            "additionalProperties": False,
        }
        validate(instance={}, schema=schema)

    response = func({}, {})
    assert response == {
        "statusCode": 400,
        "body": '{"errors": [{"message": "title: is a required property"}]}',
    }


def test_post_not_found_error_handling():
    @error_handler
    def func(event, context):
        raise PostNotFoundError("Post not found")

    response = func({}, {})
    assert response == {
        "statusCode": 404,
        "body": '{"errors": [{"message": "Post not found"}]}',
    }


def test_generic_exception_handling():
    @error_handler
    def func(event, context):
        raise Exception("A generic error occurred")

    response = func({}, {})
    assert response == {
        "statusCode": 500,
        "body": '{"errors": [{"message": "Unknown error, ctx: A generic error occurred"}]}',
    }
