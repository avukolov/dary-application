import json
from unittest.mock import MagicMock, patch

import pytest

from lambda_functions.log_in_user import handler


@pytest.fixture
def mock_event():
    return {"body": json.dumps({"username": "test_user", "password": "test_password"})}


@patch("lambda_functions.sign_up_user.boto3.client")
def test_log_in_success(mock_boto_client, mock_event: dict[str, str]):
    # Mocking the Cognito client
    mock_cognito_client = MagicMock()
    mock_boto_client.return_value = mock_cognito_client

    authentication_result = {
        "AccessToken": "some_access_token",
        "ExpiresIn": 3600,
        "TokenType": "Bearer",
        "RefreshToken": "some_refresh_token",
        "IdToken": "some_id_token",
    }

    # Mock initiate_auth response
    mock_cognito_client.initiate_auth.return_value = {
        "ResponseMetadata": {"HTTPStatusCode": 200},
        "AuthenticationResult": authentication_result,
    }

    # Call the handler
    response = handler(mock_event, None)

    assert response["statusCode"] == 201
    response_body = json.loads(response["body"])
    assert response_body == authentication_result


@patch("lambda_functions.sign_up_user.boto3.client")
@pytest.mark.parametrize(
    "params, expected_error_message",
    [
        ({}, "username: is a required property"),
        ({"username": "test_user"}, "password: is a required property"),
        ({"password": "password2"}, "username: is a required property"),
    ],
)
def test_log_in_invalid_schema(
    mock_boto_client,
    mock_event: dict[str, str],
    params,
    expected_error_message,
):
    mock_event["body"] = json.dumps(params)
    response = handler(mock_event, None)

    assert response["statusCode"] == 400

    response_body = json.loads(response["body"])
    expected_error = {
        "message": expected_error_message,
    }
    assert response_body["errors"] == [expected_error]
