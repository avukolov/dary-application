import os

import boto3
import pytest
from dotenv import load_dotenv
from moto import mock_dynamodb as dynamodb_mock


@pytest.fixture()
def mock_dynamodb():
    load_dotenv()

    with dynamodb_mock():
        dynamodb = boto3.resource("dynamodb")
        table = dynamodb.create_table(
            TableName=os.getenv("DB_TABLE_NAME"),
            KeySchema=[{"AttributeName": "id", "KeyType": "HASH"}],
            AttributeDefinitions=[
                {"AttributeName": "id", "AttributeType": "S"},
                {"AttributeName": "user_id", "AttributeType": "S"},
                {"AttributeName": "lookup", "AttributeType": "S"},
                {"AttributeName": "createdDate", "AttributeType": "S"},
            ],
            GlobalSecondaryIndexes=[
                {
                    "IndexName": "UserIdCreatedDateIndex",
                    "KeySchema": [
                        {"AttributeName": "user_id", "KeyType": "HASH"},
                        {"AttributeName": "createdDate", "KeyType": "RANGE"},
                    ],
                    "Projection": {"ProjectionType": "ALL"},
                    "ProvisionedThroughput": {
                        "ReadCapacityUnits": 1,
                        "WriteCapacityUnits": 1,
                    },
                },
                {
                    "IndexName": "LookupCreatedDateIndex",
                    "KeySchema": [
                        {"AttributeName": "lookup", "KeyType": "HASH"},
                        {"AttributeName": "createdDate", "KeyType": "RANGE"},
                    ],
                    "Projection": {"ProjectionType": "ALL"},
                    "ProvisionedThroughput": {
                        "ReadCapacityUnits": 1,
                        "WriteCapacityUnits": 1,
                    },
                },
            ],
            ProvisionedThroughput={"ReadCapacityUnits": 1, "WriteCapacityUnits": 1},
        )
        yield table
