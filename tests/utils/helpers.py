import jwt


def prepare_token(user_id, username=None):
    claims = {"sub": user_id}
    if username:
        claims["cognito:username"] = username
    return jwt.encode(claims, "secret")
