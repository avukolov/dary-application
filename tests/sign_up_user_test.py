import json
from unittest.mock import MagicMock, patch

import pytest

from lambda_functions.sign_up_user import handler


@pytest.fixture
def mock_event():
    return {"body": json.dumps({"username": "test_user", "password": "test_password"})}


@patch("lambda_functions.sign_up_user.boto3.client")
def test_handler_success(mock_boto_client, mock_event: dict[str, str]):
    # Mocking the Cognito client
    mock_cognito_client = MagicMock()
    mock_boto_client.return_value = mock_cognito_client

    # Mock sign_up response
    mock_cognito_client.sign_up.return_value = {
        "ResponseMetadata": {"HTTPStatusCode": 200},
        "UserSub": "test_user_id",
    }

    # Mock admin_confirm_sign_up response
    mock_cognito_client.admin_confirm_sign_up.return_value = {
        "ResponseMetadata": {"HTTPStatusCode": 200}
    }

    response = handler(mock_event, None)

    assert response["statusCode"] == 201
    body = json.loads(response["body"])
    assert body["user_id"] == "test_user_id"
    assert body["username"] == "test_user"


@patch("lambda_functions.sign_up_user.boto3.client")
@pytest.mark.parametrize(
    "params, expected_error_message",
    [
        ({}, "username: is a required property"),
        ({"username": "test_user"}, "password: is a required property"),
        ({"password": "password2"}, "username: is a required property"),
    ],
)
def test_handler_invalid_schema(
    mock_boto_client,
    mock_event: dict[str, str],
    params,
    expected_error_message,
):
    mock_event["body"] = json.dumps(params)
    response = handler(mock_event, None)

    assert response["statusCode"] == 400

    response_body = json.loads(response["body"])
    expected_error = {
        "message": expected_error_message,
    }
    assert response_body["errors"] == [expected_error]
