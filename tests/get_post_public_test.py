import json

from faker import Faker
from faker.providers import lorem

from common.post import Post
from lambda_functions import get_post_public
from tests.utils.dynamodb_fixtures import mock_dynamodb


# success flow
def test_get_post_found(mock_dynamodb):
    fake = Faker()
    fake.add_provider(lorem)

    title = fake.sentence()
    body = fake.paragraph()

    post_model = Post(title=title, body=body, user_id="user_id")
    mock_dynamodb.put_item(Item=json.loads(post_model.model_dump_json()))

    post_id = str(post_model.id)

    event = {"pathParameters": {"id": post_id}}
    response = get_post_public.handler(event, {})

    assert response["statusCode"] == 200

    response_body = json.loads(response["body"])
    assert response_body["id"] == post_id
    assert response_body["title"] == title
    assert response_body["body"] == body
    assert response_body["tags"] == []

    assert "createdDate" in response_body
    assert "updatedDate" in response_body

    assert "user_id" not in response_body
    assert "lookup" not in response_body


# failure flow, when post was not found by id
def test_get_post_not_found(mock_dynamodb):
    event = {"pathParameters": {"id": "non-existing-post-id"}}
    response = get_post_public.handler(event, {})

    assert response["statusCode"] == 404

    response_body = json.loads(response["body"])
    expected_message = "Post not found"
    assert response_body["errors"][0]["message"] == expected_message
