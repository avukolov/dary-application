import json
import uuid

import pytest
from faker import Faker
from faker.providers import lorem

from common.post import Post
from lambda_functions import create_post
from tests.utils.dynamodb_fixtures import mock_dynamodb
from tests.utils.helpers import prepare_token


def setup_faker():
    fake = Faker()
    fake.add_provider(lorem)
    return fake


def get_post(post_id, mock_dynamodb):
    response = mock_dynamodb.get_item(Key={"id": post_id})

    return Post(**response["Item"]) if "Item" in response else None


# success flow
def test_create_post(mock_dynamodb):
    fake = setup_faker()
    title = fake.sentence()
    body = fake.paragraph()
    tags = fake.words()

    user_id = str(uuid.uuid4())

    event = {
        "body": json.dumps({"title": title, "body": body, "tags": tags}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }

    response = create_post.handler(event, {})

    assert response["statusCode"] == 201

    # check response
    response_body = json.loads(response["body"])
    assert response_body["title"] == title
    assert response_body["body"] == body
    assert response_body["tags"] == tags
    assert "id" in response_body
    assert "user_id" not in response_body
    assert "lookup" not in response_body
    assert "createdDate" in response_body
    assert "updatedDate" in response_body

    # check that post was saved to dynamodb
    post = get_post(response_body["id"], mock_dynamodb)
    assert post.title == title
    assert post.user_id == user_id
    assert post.body == body
    assert post.tags == tags


fake = Faker()
fake.add_provider(lorem)
title = fake.sentence()
body = fake.paragraph()


# failure flow
@pytest.mark.parametrize(
    "params,expected_error_messages",
    [
        ({}, ["title: is a required property"]),
        ({"title": title}, ["body: is a required property"]),
        ({"body": body}, ["title: is a required property"]),
        ({"title": "", "body": body}, ["title: should be non-empty"]),
        ({"title": title, "body": ""}, ["body: should be non-empty"]),
        ({"title": "a" * 201, "body": body}, ["title: is too long"]),
        ({"title": title, "body": "a" * 2001}, ["body: is too long"]),
        (
            {"title": title, "body": body, "tags": 42},
            ["tags: is not valid type"],
        ),
    ],
)
def test_create_post_validation(params, expected_error_messages):
    event = {
        "body": json.dumps(params),
        "headers": {"Authorization": f"Bearer {prepare_token('user_id')}"},
    }
    response = create_post.handler(event, {})

    assert response["statusCode"] == 400

    response_body = json.loads(response["body"])
    for i in range(len(expected_error_messages)):
        assert response_body["errors"][i]["message"] == expected_error_messages[i]
