import json
import uuid

from faker import Faker
from faker.providers import lorem

from common.post import Post
from lambda_functions import get_all_posts
from tests.utils.dynamodb_fixtures import mock_dynamodb
from tests.utils.helpers import prepare_token


def setup_faker():
    fake = Faker()
    fake.add_provider(lorem)
    return fake


def create_posts(
    user_id,
    fake,
    count,
    tags=None,
):
    posts = []
    for _ in range(count):
        post = Post(
            user_id=user_id,
            title=fake.sentence(),
            body=fake.paragraph(),
            tags=tags if tags else [],
        )
        posts.append(post)
    return posts


def populate_mock_dynamodb(posts, mock_dynamodb):
    for post in posts:
        mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))


# all posts without any filters sorted by createdDate in ascending order
def test_all_posts_order(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())
    number_of_posts = 3
    current_user_posts = create_posts(user_id, fake, number_of_posts)
    populate_mock_dynamodb(current_user_posts, mock_dynamodb)

    another_user_posts = create_posts(str(uuid.uuid4()), fake, 2)
    populate_mock_dynamodb(another_user_posts, mock_dynamodb)

    event = {
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = get_all_posts.handler(event, {})

    assert response["statusCode"] == 200

    response_body = json.loads(response["body"])
    assert len(response_body) == number_of_posts

    # check that posts sorted by createdDate in ascending order
    assert response_body[0]["createdDate"] <= response_body[1]["createdDate"]
    assert response_body[1]["createdDate"] <= response_body[2]["createdDate"]

    # check post object structure
    for i in range(number_of_posts):
        assert response_body[i]["id"] == str(current_user_posts[i].id)
        assert response_body[i]["title"] == current_user_posts[i].title
        assert response_body[i]["body"] == current_user_posts[i].body
        assert response_body[i]["tags"] == []

        assert "createdDate" in response_body[i]
        assert "updatedDate" in response_body[i]
        assert "user_id" not in response_body[i]
        assert "lookup" not in response_body[i]


# filter by tags
def test_all_posts_filtered_by_tags(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())
    posts = [
        Post(
            user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=["tag1"]
        ),
        Post(user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=[]),
        Post(
            user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=["tag3"]
        ),
        Post(
            user_id=user_id,
            title=fake.sentence(),
            body=fake.paragraph(),
            tags=["tag1", "tag2"],
        ),
    ]
    expected_posts = [posts[2], posts[3]]

    populate_mock_dynamodb(posts, mock_dynamodb)

    event = {
        "queryStringParameters": {"tags": "tag3,tag2"},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = get_all_posts.handler(event, {})

    assert response["statusCode"] == 200

    response_body = json.loads(response["body"])

    assert len(response_body) == len(expected_posts)

    # check that posts sorted by createdDate in ascending order
    assert response_body[0]["createdDate"] <= response_body[1]["createdDate"]

    # check correctness of filter by tags
    for i in range(len(expected_posts)):
        assert response_body[i]["id"] == str(expected_posts[i].id)
        assert response_body[i]["title"] == expected_posts[i].title
        assert response_body[i]["body"] == expected_posts[i].body
        assert response_body[i]["tags"] == expected_posts[i].tags

        assert "createdDate" in response_body[i]
        assert "updatedDate" in response_body[i]
        assert "user_id" not in response_body[i]
        assert "lookup" not in response_body[i]


# limit
def test_all_posts_limit(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())
    number_of_posts = 5

    current_user_posts = create_posts(user_id, fake, number_of_posts)
    populate_mock_dynamodb(current_user_posts, mock_dynamodb)

    another_user_posts = create_posts(str(uuid.uuid4()), fake, 2)
    populate_mock_dynamodb(another_user_posts, mock_dynamodb)

    event = {
        "queryStringParameters": {"limit": "3"},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = get_all_posts.handler(event, {})

    assert response["statusCode"] == 200

    response_body = json.loads(response["body"])
    assert len(response_body) == 3

    # check that posts sorted by createdDate in ascending order
    assert response_body[0]["createdDate"] <= response_body[1]["createdDate"]
    assert response_body[1]["createdDate"] <= response_body[2]["createdDate"]

    # check post object structure
    for i in range(3):
        assert response_body[i]["id"] == str(current_user_posts[i].id)
        assert response_body[i]["title"] == current_user_posts[i].title
        assert response_body[i]["body"] == current_user_posts[i].body
        assert response_body[i]["tags"] == []

        assert "createdDate" in response_body[i]
        assert "updatedDate" in response_body[i]
        assert "user_id" not in response_body[i]
        assert "lookup" not in response_body[i]


# filter by tags and limit
def test_all_posts_filtered_by_tags_and_limit(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())

    posts = [
        Post(
            user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=["tag1"]
        ),
        Post(user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=[]),
        Post(
            user_id=user_id, title=fake.sentence(), body=fake.paragraph(), tags=["tag3"]
        ),
        Post(
            user_id=user_id,
            title=fake.sentence(),
            body=fake.paragraph(),
            tags=["tag1", "tag2"],
        ),
    ]
    expected_posts = [posts[2], posts[3]]

    populate_mock_dynamodb(posts, mock_dynamodb)

    event = {
        "queryStringParameters": {"tags": "tag3,tag2", "limit": "1"},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = get_all_posts.handler(event, {})

    assert response["statusCode"] == 200

    response_body = json.loads(response["body"])

    assert len(response_body) == 1

    # check correctness of filter by tags
    assert response_body[0]["id"] == str(expected_posts[0].id)
    assert response_body[0]["title"] == expected_posts[0].title
    assert response_body[0]["body"] == expected_posts[0].body
    assert response_body[0]["tags"] == expected_posts[0].tags

    assert "createdDate" in response_body[0]
    assert "updatedDate" in response_body[0]
    assert "user_id" not in response_body[0]
    assert "lookup" not in response_body[0]
