import json
import uuid
from datetime import datetime

import pytest
from faker import Faker
from faker.providers import lorem

from common.post import Post
from lambda_functions import update_post
from tests.utils.dynamodb_fixtures import mock_dynamodb
from tests.utils.helpers import prepare_token


def setup_faker():
    fake = Faker()
    fake.add_provider(lorem)
    return fake


def get_post(post_id, mock_dynamodb):
    response = mock_dynamodb.get_item(Key={"id": post_id})

    return Post(**response["Item"]) if "Item" in response else None


# success flow with valid params
def test_update_post_success(mock_dynamodb):
    fake = setup_faker()

    old_title = fake.sentence()
    new_title = fake.sentence()
    user_id = str(uuid.uuid4())

    post = Post(title=old_title, body=fake.paragraph(), user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_prev_updated_date = post.updatedDate
    post_id = str(post.id)

    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps({"title": new_title}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }

    response = update_post.handler(event, {})

    assert response["statusCode"] == 204
    assert response["body"] == ""

    retrieved_post = get_post(post_id, mock_dynamodb)
    assert retrieved_post.title == new_title
    assert retrieved_post.updatedDate != post_prev_updated_date


# success flow when update tags
def test_update_post_tags_success(mock_dynamodb):
    fake = setup_faker()

    old_tags = ["tag1", "tag2"]
    new_tags = []
    user_id = str(uuid.uuid4())

    post = Post(
        title=fake.sentence(), body=fake.paragraph(), tags=old_tags, user_id=user_id
    )
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_prev_updated_date = post.updatedDate
    post_id = str(post.id)

    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps({"tags": new_tags}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }

    response = update_post.handler(event, {})

    assert response["statusCode"] == 204
    assert response["body"] == ""

    retrieved_post = get_post(post_id, mock_dynamodb)
    assert retrieved_post.tags == new_tags
    assert retrieved_post.updatedDate != post_prev_updated_date


# success flow with valid and unsupported params
def test_update_post_success_with_unsupported_params(mock_dynamodb):
    fake = setup_faker()

    old_title = fake.sentence()
    new_title = fake.sentence()
    user_id = str(uuid.uuid4())

    post = Post(title=old_title, body=fake.paragraph(), tags=["tag1"], user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_prev_created_date = post.createdDate
    post_prev_updated_date = post.updatedDate
    post_id = str(post.id)

    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps(
            {"id": "stolen-id", "title": new_title, "createdDate": str(datetime.now())}
        ),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }

    response = update_post.handler(event, {})

    assert response["statusCode"] == 204
    assert response["body"] == ""

    retrieved_post = get_post(post_id, mock_dynamodb)
    assert str(retrieved_post.id) == post_id
    assert retrieved_post.title == new_title
    assert retrieved_post.createdDate == post_prev_created_date
    assert retrieved_post.updatedDate != post_prev_updated_date


# failure flow when post not found
def test_update_post_not_found(mock_dynamodb):
    user_id = str(uuid.uuid4())
    event = {
        "pathParameters": {"id": "non-existing-post-id"},
        "body": json.dumps({"title": "Some title"}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = update_post.handler(event, {})

    assert response["statusCode"] == 404

    response_body = json.loads(response["body"])
    expected_message = "Post not found"
    assert response_body["errors"][0]["message"] == expected_message


# failure when post does not related to current user
def test_update_post_not_related_to_user(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())
    other_user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=other_user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)
    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps({"title": "Some title"}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = update_post.handler(event, {})

    assert response["statusCode"] == 404

    response_body = json.loads(response["body"])
    expected_message = "Post not found"
    assert response_body["errors"][0]["message"] == expected_message


# failure flow with missing params
def test_update_post_missing_params(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)
    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps({}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = update_post.handler(event, {})

    assert response["statusCode"] == 400
    response_body = json.loads(response["body"])

    expected_error_message = (
        "At least one of 'title', 'body', or 'tags' must be present."
    )
    assert response_body["errors"][0]["message"] == expected_error_message


# failure flow with unsupported params
def test_update_post_unsupported_params(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)
    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps({}),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = update_post.handler(event, {})

    assert response["statusCode"] == 400
    response_body = json.loads(response["body"])

    expected_error_message = (
        "At least one of 'title', 'body', or 'tags' must be present."
    )
    assert response_body["errors"][0]["message"] == expected_error_message


@pytest.mark.parametrize(
    "params,expected_error_messages",
    [
        ({}, ["At least one of 'title', 'body', or 'tags' must be present."]),
        (
            {"id": "try-to-update-id", "createdDate": "try-to-update-createdDate"},
            ["At least one of 'title', 'body', or 'tags' must be present."],
        ),
        ({"title": ""}, ["title: is too short"]),
        ({"body": ""}, ["body: is too short"]),
        ({"title": "a" * 201}, ["title: is too long"]),
        ({"body": "a" * 2001}, ["body: is too long"]),
        ({"tags": 42}, ["tags: is not valid type"]),
    ],
)
def test_update_post_validation(mock_dynamodb, params, expected_error_messages):
    fake = setup_faker()
    user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)
    event = {
        "pathParameters": {"id": post_id},
        "body": json.dumps(params),
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = update_post.handler(event, {})

    assert response["statusCode"] == 400
    response_body = json.loads(response["body"])

    for i in range(len(expected_error_messages)):
        assert response_body["errors"][i]["message"] == expected_error_messages[i]
