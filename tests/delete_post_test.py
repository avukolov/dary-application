import json
import uuid

from faker import Faker
from faker.providers import lorem

from common.post import Post
from lambda_functions import delete_post
from tests.utils.dynamodb_fixtures import mock_dynamodb
from tests.utils.helpers import prepare_token


def setup_faker():
    fake = Faker()
    fake.add_provider(lorem)
    return fake


def get_post(post_id, mock_dynamodb):
    response = mock_dynamodb.get_item(Key={"id": post_id})

    return Post(**response["Item"]) if "Item" in response else None


# success delete post
def test_delete_post(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)

    event = {
        "pathParameters": {"id": post_id},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = delete_post.handler(event, {})

    assert response["statusCode"] == 204
    assert response["body"] == ""

    retrieved_post = get_post(post_id, mock_dynamodb)
    assert retrieved_post is None


# failure flow when post not found
def test_delete_post_not_found(mock_dynamodb):
    user_id = str(uuid.uuid4())

    event = {
        "pathParameters": {"id": "non-existing-post-id"},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = delete_post.handler(event, {})

    assert response["statusCode"] == 404

    response_body = json.loads(response["body"])

    expected_message = "Post not found"
    assert response_body["errors"][0]["message"] == expected_message


# failure flow when post does not related to current user
def test_delete_post_not_related_to_current_user(mock_dynamodb):
    fake = setup_faker()
    user_id = str(uuid.uuid4())
    other_user_id = str(uuid.uuid4())

    post = Post(title=fake.sentence(), body=fake.paragraph(), user_id=other_user_id)
    mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))

    post_id = str(post.id)

    event = {
        "pathParameters": {"id": post_id},
        "headers": {"Authorization": f"Bearer {prepare_token(user_id)}"},
    }
    response = delete_post.handler(event, {})

    assert response["statusCode"] == 404

    response_body = json.loads(response["body"])

    expected_message = "Post not found"
    assert response_body["errors"][0]["message"] == expected_message
