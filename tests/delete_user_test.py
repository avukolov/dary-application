import json
import os
from unittest import mock

import boto3
import pytest
from faker import Faker
from faker.providers import lorem
from moto import mock_cognitoidp

from common.post import Post
from lambda_functions.delete_user import handler
from tests.utils.dynamodb_fixtures import mock_dynamodb
from tests.utils.helpers import prepare_token


def create_posts(
    user_id,
    fake,
    count,
    tags=None,
):
    posts = []
    for _ in range(count):
        post = Post(
            user_id=user_id,
            title=fake.sentence(),
            body=fake.paragraph(),
            tags=tags if tags else [],
        )
        posts.append(post)
    return posts


def populate_mock_dynamodb(posts, mock_dynamodb):
    for post in posts:
        mock_dynamodb.put_item(Item=json.loads(post.model_dump_json()))


@mock.patch("lambda_functions.delete_user.LIMIT", 1)
@mock_cognitoidp
def test_delete_user_success(mock_dynamodb):
    fake = Faker()
    fake.add_provider(lorem)

    # Create a user
    username = fake.user_name()
    password = fake.password()

    cognito_client = boto3.client("cognito-idp")

    # create cognito user pool
    user_pool_id = cognito_client.create_user_pool(PoolName="TestUserPool")["UserPool"][
        "Id"
    ]

    # create cognito user pool client
    app_client = cognito_client.create_user_pool_client(
        UserPoolId=user_pool_id, ClientName="TestAppClient"
    )
    app_client_id = app_client["UserPoolClient"]["ClientId"]

    os.environ["COGNITO_USER_POOL_ID"] = user_pool_id
    os.environ["COGNITO_USER_POOL_CLIENT_ID"] = app_client_id

    create_user_response = cognito_client.sign_up(
        ClientId=app_client_id,
        Username=username,
        Password=password,
    )

    user_id = create_user_response.get("UserSub")

    # Create some posts for the user
    posts = create_posts(user_id, fake, 3)
    populate_mock_dynamodb(posts, mock_dynamodb)

    event = {
        "headers": {
            "Authorization": f"Bearer {prepare_token(user_id=user_id, username=username)}"
        },
    }

    response = handler(event, None)

    assert response["statusCode"] == 204
    assert response["body"] == ""

    # Check if the user's posts are deleted
    for post in posts:
        response = mock_dynamodb.get_item(Key={"id": str(post.id), "user_id": user_id})
        assert "Item" not in response

    # Check if the user is deleted
    cognito_response = cognito_client.list_users(UserPoolId=user_pool_id)
    assert len(cognito_response["Users"]) == 0
